#pragma once

// Virgin Milky notes:
//
// If we want the master (= the side that has the usb plugged into it)
// to be the right hand, we'll have to set this constant.
//
// If we want both sides to behave as we expect, we have to flash the left
// side without this define, then flash the right hand with the define on.
// That means that we'll be flashing two different images to each side.
//
// https://docs.qmk.fm#/feature_split_keyboard?id=handedness-by-define
//
// There is an another way -- by setting a persistent variable
// in the EEPROM memory. But I haven't checked that out yet, this good enough.

// By the way -- it's also great for AB-testing some changes because
// you can always plug into the other unchanged half if something
// acts up or is blatantly wrong. Neat!

//#define MASTER_LEFT
//#define MASTER_RIGHT

// Chad Sneky define:
#define EE_HANDS


//-------------------------------------------------------------------------


//  --- Pimoroni ---
#define TAPPING_FORCE_HOLD
//#define TAPPING_TERM 100 // No idea why pimo sets this up this way
// Snekys cripple tapping term
//#define TAPPING_TERM 300

/*
#define POINTING_DEVICE_ROTATION_90 	//(Optional) Rotates the X and Y data by 90 degrees.
#define POINTING_DEVICE_ROTATION_180 	//(Optional) Rotates the X and Y data by 180 degrees.
#define POINTING_DEVICE_ROTATION_270 	//(Optional) Rotates the X and Y data by 270 degrees.
*/
#define POINTING_DEVICE_INVERT_X 	//(Optional) Inverts the X axis report.
#define POINTING_DEVICE_INVERT_Y 	//(Optional) Inverts the Y axis report.

#define PIMORONI_TRACKBALL_INTERVAL_MS 8
//change update rate for trackball, from testing update rate seems to be limited to around 250. Updating more frequently will slow down the keyboard.
#define PIMORONI_TRACKBALL_SCALE 4
//5 is default, I personally prefer 3 which feels a little smoother.
#define PIMORONI_TRACKBALL_DEBOUNCE_CYCLES 20
//number of cycles to stop motion after clicking. 20 x 8 (INTERVAL) so 160ms.
#define PIMORONI_TRACKBALL_ERROR_COUNT 10
//stops trying to read from the pimoroni after this many faults.


//  --- HOME ROW MODIFIER SETUP ---

// https://precondition.github.io/home-row-mods
// https://github.com/qmk/qmk_firmware/blob/master/docs/tap_hold.md

//  Sneky: I will enable these as I go

// Configure the global tapping term (default=200ms)
//
// You can also do a huge number to make it near-impossible to use
// unless it goes through PERMISSIVE_HOLD, which works very well.
//
// Setting a 500 is a bit too much. The issue is in the pinkies, mostly.
// NOTE:
//   You can always set the timer per-key.
// NOTE:
//   This also affects other dual-function-keys like
//   `` LT(__SYM_NUM__,KC_SPC) .. Numbers on hold, Space on tap ``
//   which is a big problem.
//   Separating the global settings and home-row settings
//   is recommended. Requires doing c-code though...
//
// That said, having a dedicated shift on the thumbs is better
// than trying to get a PERMISSIVE_HOLD to trigger at speed,
// so we don't need to worry about this too much.
//
// If you want to quickly trigger a plain up-down of a mod
// (peek start by Winkey or get the Alt menu to disappear)
// use a MO(__OS__) and trigger it there without quirks.
//
#define TAPPING_TERM 200

// WE WILL USE PER-KEY TO SET HOMEROW.
// Look in `keymap.c` to check details.
//#define TAPPING_TERM_PER_KEY

// Permissive hold helps by defining a nested up>down as a keypress in
// a modifier key.
// (   nested O in A   =   Down[A],  Down[O], Up[O],  Up[A]   )
//#define PERMISSIVE_HOLD

// Prevent normal rollover on alphas from accidentally triggering mods.
//#define IGNORE_MOD_TAP_INTERRUPT

// Enable rapid switch from tap to hold, disables double tap hold auto-repeat. -- yeah this was breaking my _bsp >:(  ....not?!
//#define TAPPING_FORCE_HOLD



//  --- MOVING THE MOUSE ---

// Controlling the mouse with the keyboard has many options that can be
// configured via these defines.
//
// Check the docs here:
//   https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/feature_mouse_keys

// Delay between press and cursor move. Let's make this as short as possible
// otherwise we'll be moving way too much on the smallest keypress. (default=300)
// Anything below 32 is pretty much just a nuisance when aiming
// for pixel-perfect borders etc.

#define MOUSEKEY_DELAY 32

// "Step size" I guess multiplies every movement of the mouse.
// For gaming, I find that you need a small value to not notice
// any stutter in 3D mouse-look -- you can easily notice the difference.
// I don't think setting a one(1) helps at all, but 5 is too high.
// When modifying, make sure to change the interval and/or the max speed.
// (default=5)

#define MOUSEKEY_MOVE_DELTA 3

// As in 60 fps, dunno why its not default. Without it, the mouse moves
// every few frames which feels absolutely awful. The point is probably
// to make you used to not holding but pressing until you're where you want,
// but that's not useable! (default=50)
// Note that setting it >=10 makes it jitter weirdly in UI,
// I don't know what is up with that.

#define MOUSEKEY_INTERVAL 16

// Pretty much the inverse(?) acceleration. If it's really high, the cursor
// takes longer to speed up. Use the speed mods to get to higher speeds. (default=20)

#define MOUSEKEY_TIME_TO_MAX 20

// Lower interval >> more speed. (default=10)

#define MOUSEKEY_MAX_SPEED 10



//  --- SCROLLING! ---

// Delay between pressing a wheel key and wheel movement. (default=300)

#define MOUSEKEY_WHEEL_DELAY 20

// Time between wheel movements. (default=100)

#define MOUSEKEY_WHEEL_INTERVAL 32

// Maximum number of scroll steps per scroll action. (default=8)

#define MOUSEKEY_WHEEL_MAX_SPEED 4

// Time until maximum scroll speed is reached. (default=40)
// NEEDS UPDATE TO WORK:
//   Use a zero to disable acceleration altogether; keeping all scrolls
//   the same distance.

#define MOUSEKEY_WHEEL_TIME_TO_MAX 40

// -----------------------------------------------------------

// For some reason, the keeb has it disabled? What's the point?
#undef NO_ACTION_MACRO

#ifdef RGBLIGHT_ENABLE
    #undef RGBLED_NUM
    #define RGBLIGHT_ANIMATIONS
    #define RGBLED_NUM 27
    #define RGBLIGHT_LIMIT_VAL 120
    #define RGBLIGHT_HUE_STEP 10
    #define RGBLIGHT_SAT_STEP 17
    #define RGBLIGHT_VAL_STEP 17
#endif

//#define OLED_FONT_H "keyboards/crkbd/lib/glcdfont.c"
#define OLED_BRIGHTNESS 5
