
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  // ========================================================================================================
  [_HH] = LAYOUT_split_3x5_3(
     _medi,  undsc,  comma, t_alt,   _esc,       _del,   _tab,  dot,   dont,  _medi,
     t_ctl,     _S,     _R,    _T,   _ent,       _bsp,   _N,    _E,    _A,    MO(_HHcz),
      dont,     _G,     _L,    _D,   _ent,       _bsp,   _U,    _I,    _O,    _Y,
                     _symb,   _x2,  _spec,       t_sft,  _spc,  t_sft
  ),
  [_HH2] = LAYOUT_split_3x5_3(
     HHHH, oooo, oooo, oooo,  oooo,         oooo,  oooo, oooo, oooo,  HHHH,
     oooo,   _F,   _P,   _C,  oooo,         oooo,  _M,   dot,   _K,    oooo,
       _Q,   _J,   _V,   _B,  oooo,         oooo,  _H,    _W,   _Z,    _X,
                  oooo, oooo, oooo,         oooo,  oooo, oooo
  ),
  [_HHcz] = LAYOUT_split_3x5_3(
    ____,   M_ccc,  M_rrr,  M_ccc,    _esc,        _del,  M_u_c,  M_u_k,  M_ooo,  ____,
    g_kase, M_aaa,  M_sss,  M_ttt, KC_CAPS,        _bsp,  M_nnn,  M_e_h,  M_iii,  g_kase, 
    ____,   ____,    ____,  M_ddd, KC_LCTL,        _bsp,  M_zzz,  M_e_c,  M_yyy,  ____,
                     ____,   ____,    ____,      FAKESHIFT,  _spc,  FAKESHIFT
  ),
  // ========================================================================================================
  [_OS_Ultra] = LAYOUT_split_3x5_3(
    HHHH,   ms_3,       ms_2,      ms_1,  t_gui,         _tab,   undo,   copy,   cut,    HHHH,
    t_ctl,  KC_HOME,    KC_UP,  KC_LEFT,   _ent,         _bsp,   ms_l,   ms_u,   ms_r,   HHHH, 
    t_sft,  KC_END,   KC_DOWN, KC_RIGHT,   _esc,         _del,   scr_d,  ms_d,   scr_u,  t_gui,
    
                       _symb,     t_alt,  _spec,         t_sft,  _spc,   t_sft                
  ),
  [_Special] = LAYOUT_split_3x5_3(

    KC_F1,    KC_F2,    KC_F3,         KC_F4,    KC_F5,        KC_F6,      KC_F7,    KC_F8,    KC_F9,  KC_F10, 
     ____,  KC_VOLD,  KC_VOLU,        KC_F11,   KC_F12,        KC_INS,     KC_CAPS,  CL_SWAP,  ____,   HHHH,
    RESET,  KC_BRID,  KC_BRIU,   LALT(KC_F4),  BL_TOGG,        KC_NLCK,    KC_LCTL,  CL_NORM,  ____,   KC_PSCR,
                         HHHH,  MO(_Special),     HHHH,        HHHH,       HHHH,     HHHH                      
  ), 
  // ========================================================================================================
  [_Sym] = LAYOUT_split_3x5_3(
                              
   dollar,  comma,   curly_l,  curly_r,    amp,        plus,  bquot,   dont,   quot,    ques,
    semic,  slash,     par_l,    par_r,   pipe,        _bsp,  equ,     colon,  bslash,  argh,
     HHHH,   ____,    brak_l,   brak_r,  squig,        star,  minus,   htm_l,  htm_r,   HHHH,
                       _symb,     ____,  _symb,        MO(_Num), _spc,  MO(_Num)                   
  ),
  [_Num] = LAYOUT_split_3x5_3(
                              
    dollar,  comma,  curly_l,  curly_r,        at,        plus,    _7,    _8,    _9,  dot,       
      perc,  hasht,    par_l,    par_r,      _ent,        _bsp,    _4,    _5,    _6,  _0,
      HHHH,   ____,   brak_l,   brak_r,      chev,        star,    _1,    _2,    _3,  HHHH,
                       _symb,     ____,     _symb,        MO(_Num), _spc,  MO(_Num)                   
  ),
  // ========================================================================================================
  [_Game] = LAYOUT_split_3x5_3(
                               
     KC_TAB,  ___q,  ___w,  ___e, ___r,        ___t,  ___y,  ___u,  ___o,   ___p,
    KC_LSFT,  ___a,  ___s,  ___d, ___f,        ___g,  ___h,  ___j,  ___k,   ___l,
    KC_LCTL,  ___z,  ___x,  ___c, ___v,        ___b,  ___n,  ___m,   dot,  comma,
        OSL(_GameX),  KC_SPC,  KC_BTN1,        ____,  ____,  ____                
  ),
  [_GameX] = LAYOUT_split_3x5_3(
    KC_6,     KC_7,    KC_8,    KC_9,    KC_0,      ____,     KC_WH_U,  KC_MS_U,  KC_WH_D,  KC_ESC,  
    KC_1,     KC_2,    KC_3,    KC_4,    KC_5,      ____,     KC_MS_L,  KC_MS_D,  KC_MS_R,  KC_ACL2, 
    ____,  KC_LALT,  KC_TAB,  KC_ESC,  KC_ENT,      ____,     HHHH,     KC_BTN3,  ____,     HHHH,    
                       oooo,    ____,    HHHH,      KC_BTN2,  KC_BTN1,  KC_BTN3                      
  ),
  // ======================================================================================================== 
  [_Media] = LAYOUT_split_3x5_3( 
       HHHH,  scr_d,    ms_u,     scr_u,        KC_ESC,           KC_ESC,       scr_d,    ms_u,     scr_u,    HHHH,
     KC_SPC,  ms_l,     ms_d,     ms_r,         KC_ENT,           KC_ENT,       ms_l,     ms_d,     ms_r,     KC_SPC,
    KC_BTN2,  KC_LSFT,  KC_LALT,  KC_TAB,       KC_F11,           KC_F11,       KC_TAB,   KC_LALT,  KC_LSFT,  KC_BTN2,
                         KC_SPC,  KC_BTN1,  MO(_MediaX),          MO(_MediaX),  KC_BTN1,  KC_SPC

  ),
  [_MediaX] = LAYOUT_split_3x5_3(
       HHHH,  KC_HOME,    KC_UP,        KC_END,  KC_VOLU,        KC_VOLU,  KC_HOME,      KC_UP,    KC_END,   HHHH,
     KC_SPC,  KC_LEFT,  KC_DOWN,      KC_RIGHT,  KC_VOLD,        KC_VOLD,  KC_LEFT,      KC_DOWN,  KC_RIGHT, KC_SPC,
    KC_BTN3,  KC_LGUI,  KC_BRID,       KC_BRIU,  KC_SLEP,        KC_SLEP,  KC_BRID,      KC_BRIU,  KC_LGUI,  KC_BTN3,
                           HHHH,  MO(_Special),     oooo,        oooo,     MO(_Special), HHHH

  ),
  // ========================================================================================================
  [_Lock] = LAYOUT_split_3x5_3( // using some combos here -- they're pretty great for this! 
    ____, ____, Unl_1, Unl_1, ____,        ____, Unl_1, Unl_1, ____, ____, 
    ____, ____, Unl_2, Unl_2, ____,        ____, Unl_2, Unl_2, ____, ____, 
    ____, ____, ____,  _UP_,  ____,        ____, _UP_,  ____,  ____, ____, 
                ____,  ____,  ____,        ____, ____,  ____   
  ),
  [_Unlock] = LAYOUT_split_3x5_3( // and there's a fallback without combos 
    _DW_, _DW_, _DW_, _DW_, _DW_,        _DW_, _DW_, _DW_, _DW_, _DW_, 
    _DW_, _DW_, _DW_, ____, _DW_,        _DW_, ____, _DW_, _DW_, _DW_, 
    _DW_, _DW_, HHHH, _DW_, KC_SLEP,  KC_SLEP, _DW_, HHHH, _DW_, _DW_, 
                _DW_, _DW_, _DW_,        _DW_, _DW_, _DW_
  ),
};
