
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
// ========================================================================================================
  [_HH] = LAYOUT(
    KC_LGUI,  _spec,  _game,  _medi,  ____,  _esc,                                     KC_HOME,         carka,  hacek,  krouzek,  g_kase,  KC_RGUI,
    KC_LALT,     _Q,     _W,     _F,    _P,    _B,                                     _J,              _L,     _U,     _Y,       dont,    KC_RALT,
    KC_LSFT,     _A,     _R,     _S,    _T,    _G,                                     _M,              _N,     _E,     _I,       _O,      KC_RSFT,
    KC_LCTL,     _Z,     _X,     _C,    _D,    _V,  ____,        KC_MPLY,              _K,              _H,     comma,  dot,      minus,   KC_RCTL,
                              ____,  _num,  _sym,   _os,                _bsp,  _spc,  ____,   _spec
),
// ========================================================================================================
/*    [_Special] = LAYOUT(
     ____,     ____,     ____,         ____,   ____,          ____,                      ____,   ____,   ____,     ____,    ____,    ____,
    KC_F1,    KC_F2,    KC_F3,        KC_F4,  KC_F5,         KC_F6,                      KC_F7,  KC_F8,  KC_F9,    KC_F10,  KC_F11,  KC_F12,
     ____,  KC_VOLD,  KC_VOLU,         ____,   ____,          ____,                      ____,   ____,   KC_CAPS,  ____,    ____,    ____,
     ____,  KC_BRID,  KC_BRIU,  LALT(KC_F4),   ____,          ____,  ____,        ____,  ____,   ____,   ____,     ____,    ____,    KC_PSCR,
                                       HHHH,   HHHH,  MO(_Special),  HHHH,        HHHH,  HHHH,   HHHH,   HHHH
    ),
*/

    [_Special] = LAYOUT(
     ____,     ____,     ____,         ____,   ____,          ____,                      ____,   ____,   ____,     ____,    ____,    ____,
     ____,     KC_F12,  KC_F7,  KC_F8,  KC_F9,         ____,                      KC_F7,  KC_F8,  KC_F9,    KC_F10,  KC_F11,  KC_F12,
     ____,     KC_F11,  KC_F4,  KC_F5,  KC_F6,          ____,                      ____,   ____,   KC_CAPS,  ____,    ____,    ____,
     ____,     KC_F10,  KC_F1,  KC_F2,  KC_F3,          ____,  ____,        ____,  ____,   ____,   ____,     ____,    ____,    KC_PSCR,
                                       HHHH,   HHHH,  MO(_Special),  HHHH,        HHHH,  HHHH,   HHHH,   HHHH
    ),

// ========================================================================================================
    [_OS_Ultra] = LAYOUT(
    KC_LGUI,   ____,     ____,   ____,    ____,   ____,                       ____,  ____,   ____,  ____,   ____,   KC_RGUI,
    KC_LCTL,   HHHH,  KC_HOME,     _u,  KC_END,  t_gui,                       _tab,  scr_u,  ms_u,  scr_d,  ____,   KC_RCTL,
    KC_LSFT,  t_ctl,       _l,     _d,      _r,   _ent,                       _bsp,  ms_l,   ms_d,  ms_r,   ____,   KC_RSFT,
    KC_LALT,  t_sft,     copy,   save,    ____,   _esc,   ____,        ____,  _del,  ____,   undo,  find,   t_gui,  KC_RALT,
                                t_alt,   t_sft,  t_ctl,  t_gui,        ms_2,  ms_1,  ms_3,   t_sft
 ),


/* milky's woke os layer... but i'm not ready for it
    [_OS_Ultra] = LAYOUT(
     ____,     ____,  ____,   ____,   ____,   ____,                       ____,  ____,   ____,   ____,  ____,   ____,
     HHHH,    t_alt,  copy,   save,  t_gui,   ____,                       ____,  t_gui,  _tab,   undo,  find,   HHHH,
    t_ctl,  KC_HOME,    _u,     _l,   _ent,   ____,                       ____,  _bsp,   ms_l,   ms_u,  ms_r,   HHHH,
    t_sft,   KC_END,    _d,     _r,   _esc,   ____,  ____,        ____,   ____,  _del,   scr_d,  ms_d,  scr_u,  t_gui,
                             t_alt,  t_sft,  t_ctl,  ____,        t_ctl,  _bsp,  _spc,   t_sft
    ),
*/
// ========================================================================================================
//                |
//      | ? [ ] ^ |   _ * # $ "
//      & @ ( ) ; |     = : + !
//      ~ ; { } % |   ` \ < > /
//

  [_Sym] = LAYOUT(
    KC_LGUI,   ____,   ____,     ____,     ____,  ____,                      ____,   ____,    ____,   ____,    ____,   KC_RGUI,
    KC_LALT,   pipe,   ques,   brak_l,   brak_r,  chev,                      undsc,  star,    hasht,  dollar,  quot,   KC_RALT,
    KC_LSFT,    amp,     at,    par_l,    par_r, semic,                      _bsp,   equ,     colon,  plus,    argh,   KC_RSFT,
    KC_LCTL,  squig,   ____,  curly_l,  curly_r,  perc,  ____,        ____,  bquot,  bslash,  htm_l,  htm_r,   slash,  KC_RCTL,
                              ____,     ____,  ____,  ____,        ____,  ____,   _spc,    ____
  ),
  [_Num] = LAYOUT(
    KC_LGUI,   ____,   ____,    ____,    ____,  ____,                    KC_NUM,  ____,  _KP_SLSH,  ____,  ____,     KC_RGUI,
    KC_LALT,   ____,   chev,      at,    ____,  ____,                      _tab,  _7,    _8,          _9,    _ent,   KC_RALT,
    KC_LSFT,   perc,  undsc,   par_l,   par_r,  _ent,                      _bsp,  _4,    _5,          _6,    _0,     KC_RSFT,
    KC_LCTL,  bquot,   ____,  brak_l,  brak_r,  ____,  ____,        ____,  _stb,  _1,    _2,          _3, _KP_DOT,   KC_RCTL,
                             ____,    ____,  ____,  ____,        ____,  ____,  _spc,  ____

  ),
// ========================================================================================================
// basic ass colemak
  [_Game] = LAYOUT(
         TO(_HH),  KC_1,  KC_2,  KC_3,    KC_4,     KC_5,                               KC_6,    KC_7,      KC_8,     KC_9,    KC_0,    KC_BSPC,
          KC_TAB,  KC_Q,  KC_W,  KC_F,    KC_P,     KC_B,                               KC_J,    KC_L,      KC_U,     KC_Y,    dont,     KC_DEL,
         KC_LGUI,  KC_A,  KC_R,  KC_S,    KC_T,     KC_G,                               KC_M,    KC_N,      KC_E,     KC_I,    KC_O,     KC_ENT,
         KC_LCTL,  KC_Z,  KC_X,  KC_C,    KC_D,     KC_V,        ____,        KC_MPLY,  KC_K,    KC_H,      KC_COMM,  KC_DOT,  KC_SLSH,  ____,
                                 KC_LCTL,  KC_ESC,  KC_LSFT,  MO(_GameX),        KC_BSPC,  KC_SPC,  KC_LCTRL,  t_alt
  ),
    [_GameX] = LAYOUT(
            ____,  KC_6,  KC_7,     KC_8,    KC_9,     KC_0,                         ____,  ____,  ____,  ____,  ____,  ____,
         KC_BSPC,  KC_J,  KC_L,     KC_U,    KC_Y,     dont,                         ____,  ____,  ____,  ____,  ____,  ____,
          KC_SPC,  KC_M,  KC_N,     KC_E,    KC_I,     KC_O,                         ____,  ____,  ____,  ____,  ____,  ____,
    TO(_GameFPS),  KC_K,  KC_H,  KC_COMM,  KC_DOT,  KC_SLSH,  ____,        KC_MPLY,  ____,  ____,  ____,  ____,  ____,  ____,
                                    ____,    ____,  KC_LSFT,  ____,        ____,     ____,  ____,  ____

  ),
// qwerty with specific stuff derived from borderlands and cyberpunk prolly
  #define qload KC_F5
  #define qsave KC_F4
   [_GameFPS] = LAYOUT(
    TO(_HH),     KC_1,  KC_2,      KC_3,    KC_4,    KC_5,                                   KC_6,    KC_7,     KC_8,    KC_9,    KC_0,      TO(_HH),
      qload,   KC_TAB,  KC_Q,      KC_W,    KC_E,    KC_R,                                   KC_T,    KC_Y,     KC_U,    KC_I,    KC_O,      KC_P,
    KC_LALT,  KC_LSFT,  KC_A,      KC_S,    KC_D,    KC_F,                                   KC_G,    KC_H,     KC_J,    KC_K,    KC_L,      KC_ENTER,
    KC_LGUI,  KC_LCTL,  KC_Z,      KC_X,    KC_C,    KC_V,         KC_LBRC,        KC_MPLY,  KC_B,    KC_N,     KC_M,    KC_DOT,  KC_COMMA,  ____,
                               KC_ENTER,  KC_ESC,  KC_SPC,  MO(_GameFPS_X),        KC_ENT,   KC_SPC,  KC_BSPC,  KC_RGUI
   ),
   [_GameFPS_X] = LAYOUT(
    TO(_HH),  KC_6,  KC_7,      KC_8,    KC_9,      KC_0,                         ____,  ____,  ____,  ____,  ____,  ____,
      qsave,  KC_T,  KC_Y,      KC_U,    KC_I,      KC_O,                         ____,  ____,  ____,  ____,  ____,  ____,
    KC_BSPC,  KC_G,  KC_H,      KC_J,    KC_K,      KC_L,                         ____,  ____,  ____,  ____,  ____,  ____,
    KC_LGUI,  KC_B,  KC_N,      KC_M,  KC_DOT,  KC_COMMA,  ____,        KC_MPLY,  ____,  ____,  ____,  ____,  ____,  ____,
                            KC_ENTER,  KC_ESC,    KC_SPC,  ____,        ____,     ____,  ____,  ____
   ),

// ========================================================================================================
  [_Media] = LAYOUT(
    ____,     ____,     ____,     ____,    ____,     ____,                                    ____,     ____,    ____,     ____,     ____,     ____,
    ____,     HHHH,    scr_d,     ms_u,   scr_u,   KC_ESC,                                    KC_ESC,   scr_d,   ms_u,     scr_u,    HHHH,     ____,
    ____,   KC_SPC,     ms_l,     ms_d,    ms_r,   KC_ENT,                                    KC_ENT,   ms_l,    ms_d,     ms_r,     KC_SPC,   ____,
    ____,  KC_BTN2,  KC_LSFT,  KC_LALT,  KC_TAB,   KC_F11,         ____,        ____,         KC_F11,   KC_TAB,  KC_LALT,  KC_LSFT,  KC_BTN2,  ____,
                                  ____,  KC_SPC,  KC_BTN1,  MO(_MediaX),        MO(_MediaX),  KC_BTN1,  KC_SPC,  ____
  ),
  [_MediaX] = LAYOUT(
    ____,     ____,     ____,     ____,     ____,          ____,                      ____,          ____,     ____,     ____,     ____,    ____,
    ____,     HHHH,  KC_HOME,     _upp,   KC_END,       KC_VOLU,                      KC_VOLU,       KC_HOME,  _upp,     KC_END,   HHHH,    ____,
    ____,   KC_SPC,     _lft,     _dwn,     _rgt,       KC_VOLD,                      KC_VOLD,       _lft,     _dwn,     _rgt,     KC_SPC,  ____,
    ____,  KC_BTN3,  KC_LGUI,  KC_BRID,  KC_BRIU,       KC_SLEP,  ____,        ____,  KC_SLEP,       KC_BRID,  KC_BRIU,  KC_LGUI,  ____,    KC_BTN3,
                                  ____,     HHHH,  MO(_Special),  oooo,        oooo,  MO(_Special),  HHHH,     ____
  ),
// ========================================================================================================
 };

/*
// ========================================================================================================
  [_] = LAYOUT(
    ____,  ____,  ____,  ____,  ____,  ____,                      ____,  ____,  ____,  ____,  ____,  ____,
    ____,  ____,  ____,  ____,  ____,  ____,                      ____,  ____,  ____,  ____,  ____,  ____,
    ____,  ____,  ____,  ____,  ____,  ____,                      ____,  ____,  ____,  ____,  ____,  ____,
    ____,  ____,  ____,  ____,  ____,  ____,  ____,        ____,  ____,  ____,  ____,  ____,  ____,  ____,
                         ____,  ____,  ____,  ____,        ____,  ____,  ____,  ____

  ),
*/
