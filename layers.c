enum layer_order {

    // NOTE:  `map.c:keymaps` array only fills the indexes,
    //   the order is defined here, always.
    //   Keeping the order in the array definition is entirely optional.

    // NOTE:  MO/LT can only reach higher layers, it can't go lower.
    //   Keep that in mind when ordering.

    // NOTE:  This affects how `oooo` (KC_TRNS) fallthroughs work,
    //   so be careful when you lean on it!


    // The home alphas -- most of typing is spent here.
    _HH, _HH2, _HHcz,

    // The copypasting, text selection and arrows, mouse and so on
    _OS_Ultra, // arrows, mouse, copypaste, undo and GUI
    _Special,  // Fkeys, media keys and so on. Also has the reset button.

    // Old symbols remain for now -- I am used to them way too much
    _Sym, _Num, _Macros,

    // Game layer with the usual QWERTY without fancy functions.
    // Supports left-hand-only operation.
    _Game,  _GameX,

    // Game layer with QWERTY with two pinky utility columns...
    //  qload   TAB Q   ...
    //  ALT     SFT A   ...
    //  GUI     CTL Z   ...
    _GameFPS, _GameFPS_X,

    // One handed media layer with arrows, mouse
    // and volume, alt tabbing, F11, sleep, brightness etc.
    // in easy-to-access places.
    // Should be "mirrored" to both hands the same way to allow
    // using only one connected half of the keyboard to do everything.
    _Media, _MediaX,
    _Unlock, //--
    _Lock, //--
};
