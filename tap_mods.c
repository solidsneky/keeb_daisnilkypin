
enum tap_mods {
    t_alt = OSM(MOD_LALT),
    t_gui = OSM(MOD_LGUI),
    t_sft = OSM(MOD_LSFT),
    t_ctl = OSM(MOD_RCTL),
};

// HOME ROW MODIFIERS
// https://precondition.github.io/home-row-mods

#define GU_A LGUI_T(KC_A)
#define AL_R LALT_T(KC_R)
#define SH_S LSFT_T(KC_S)
#define CT_T RCTL_T(KC_T)

#define CT_N RCTL_T(KC_N)
#define SH_E RSFT_T(KC_E)
#define AL_I LALT_T(KC_I)
#define GU_O RGUI_T(KC_O)

// If we have the #define set correctly, we can do per-key
// instead of global setting for tapping term.
// The global is shared to LT() calls sometimes,
// so it's good to set the home row separately from that.
// You can also use the `TAPPING_TERM` const to set things relative
// to the global setting.
uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) { // QMK

    if (false) { // Old colemak homerows
        bool gottem = false;
        uint16_t home[8] = {
            GU_A, AL_R, SH_S, CT_T,
            CT_N, SH_E, AL_I, GU_O,
        };
        for (int i = 0; i < 8; i++) {
            if (keycode == home[i]) {
                gottem = true;
            }
        }
        if (gottem) {
            return TAPPING_TERM * 3;
        }
    }

    return TAPPING_TERM;
};

// Kind of like home row modifiers, I use switching to the OS layer.
// This doesn't use the same mechanism and doesn't prevent mismatch
// when rolling.
// Use only on keys that aren't often rolled with other keys.
#define OS_T(kc) LT(_OS, kc)

