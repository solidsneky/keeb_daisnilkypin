#include QMK_KEYBOARD_H

/******************************************************************************

                              THE ROOT FILE
                                                 *cpd frm milky <3
  ...modded by sneky (  ° <°)
*******************************************************************************

    This is the file that QMK loads when it wants to compile a `user`
    component of a keyboard.
    It looks at
    - `***_user()` functions
    - the `const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {`
      layer definition array
    - and some other things, usually related to optional features (oled, combos)

    Code files are included directly. No other files in the folder
    should have these kinds of includes.
    I include the `.c` files directly, as it improves compilation
    speed and doesn't force me to write a `.h` file for every `.c` file.
    Because that would be stupid. No idea why people subject themselves
    to such torture.

    One thing to note though...
        The file includes need to be ordered by dependency.

    That is not managed, so it is up to us to take care of it.
    But it's so easy to do that I am not bothered by it at all -- a keyboard
    won't require any complexity anyway.

    Also, it makes managing dead code easier -- just comment that stuff out
    and brace for compilation errors. Easy.
*/

// the layer order definition and naming
#include "layers.c"
// has any aliases and custom keycodes used throughout
#include "custom_codes.c"
// home row mods and others live here -- not using for now
//#include "tap_mods.c"
// defines the layers via keycode arrays
// all keycodes for the map must be defined before this point
#include "map.c"

// map definition can have combo definition -- include later.
// I dont need this just yet
// #include "combos.c"


// all keycodes, combos, variables and definitions should be
// defined by this point
// this is a post-process step interjecting and modifying
// anything specified under it
// i dont need this either, i --hope--
//#include "process.c"



// Optional OLED stuff -- Sneky's is on both hands
#ifdef    OLED_ENABLE
//#ifdef    MASTER_LEFT
#include <stdio.h>
#include "oled.c"
//#endif // MASTER_LEFT
#endif // OLED_ENABLE


// Printing to the system console, if the system is listening for the data.
// The feature is usually too large to enable, but if we disable the OLED,
// it might fit on the promicro.
//   https://github.com/qmk/qmk_firmware/blob/master/docs/faq_debug.md
#ifdef CONSOLE_ENABLE
void keyboard_post_init_user(void) {
    // debug_enable=true;
    // debug_matrix=true;
    // debug_keyboard=true;
    // debug_mouse=true;
}
#endif // CONSOLE_ENABLE
