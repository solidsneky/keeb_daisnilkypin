
// Omitting Milky's custom keycode stuff, for I don't see a use for it
// .......yet


//
//  And aliases, with all their goddamn drawbacks...
//


// aliases for frequently used keys
#define ____ KC_NO   // do nothing (still affects one-shot stuff though!!)
#define HHHH TO(_HH) // "to home layer"
#define oooo KC_TRNS // fallthrough and use a "lower layer"; see `enum layer_order`

// Locking mechanism
// Sneky doesn't need it for now
//#define _UP_ MO(_Unlock)
//#define _DW_ TO(_Lock)

// for some reason, QMK didn't have a CTRL+GUI predefined... what gives?
#define WORKSP(kc) (QK_RCTL | QK_LGUI | (kc))

enum alpha_aliases {
    // These are just aliases and nothing else,
    // they can be used interchangably with KC_* in any case.

    _A = KC_A,
    _B = KC_B,
    _C = KC_C,
    _D = KC_D,
    _E = KC_E,
    _F = KC_F,
    _G = KC_G,
    _H = KC_H,
    _I = KC_I,
    _J = KC_J,
    _K = KC_K,
    _L = KC_L,
    _M = KC_M,
    _N = KC_N,
    _O = KC_O,
    _P = KC_P,
    _Q = KC_Q,
    _R = KC_R,
    _S = KC_S,
    _T = KC_T,
    _U = KC_U,
    _V = KC_V,
    _W = KC_W,
    _X = KC_X,
    _Y = KC_Y,
    _Z = KC_Z,

    // nums too, why not
    // dont forget to include LSFT
    // bc of how the czech keyboard works os-wise
//    _0 = LSFT(KC_0),
//    _1 = LSFT(KC_1),
//    _2 = LSFT(KC_2),
//    _3 = LSFT(KC_3),
//    _4 = LSFT(KC_4),
//    _5 = LSFT(KC_5),
//    _6 = LSFT(KC_6),
//    _7 = LSFT(KC_7),
//    _8 = LSFT(KC_8),
//    _9 = LSFT(KC_9),

    _0 = KC_KP_0,
    _1 = KC_KP_1,
    _2 = KC_KP_2,
    _3 = KC_KP_3,
    _4 = KC_KP_4,
    _5 = KC_KP_5,
    _6 = KC_KP_6,
    _7 = KC_KP_7,
    _8 = KC_KP_8,
    _9 = KC_KP_9,
    _KP_SLSH = KC_KP_SLASH,
    _KP_DOT = KC_KP_DOT,
// omitting this bc it seems to be related to
// homerow mods only anyway. maybe later
/*
    #ifdef MASTER_RIGHT
    // SYM NUM OS   |   OS NUM SYM
    // s   r   t    |    n   e   a
    // g   l   d    |    u   i   o
    _A = LT(_Sym, KC_A),
    _B = KC_B,
    _C = KC_C,
    _D = KC_D,
    _E = LT(_Num, KC_E),
    _F = KC_F,
    _G = KC_G,
    _H = KC_H,
    _I = LT(_Num, KC_I),
    _J = KC_J,
    _K = KC_K,
    _L = LT(_Num, KC_L),
    _M = KC_M,
    _N = LT(_OS_Ultra, KC_N),
    _O = KC_O,
    _P = KC_P,
    _Q = KC_Q,
    _R = LT(_Num, KC_R),
    _S = LT(_Sym, KC_S),
    _T = LT(_OS_Ultra, KC_T),
    _U = KC_U,
    _V = KC_V,
    _W = KC_W,
    _X = KC_X,
    _Y = KC_Y,
    _Z = KC_Z,
    #endif
*/
};

enum mouse_aliases {
    scr_u = KC_WH_U,
    scr_d = KC_WH_D,
    ms_u  = KC_MS_U,
    ms_d  = KC_MS_D,
    ms_l  = KC_MS_L,
    ms_r  = KC_MS_R,
    ms_1  = KC_BTN1,
    ms_2  = KC_BTN2,
    ms_3  = KC_BTN3,
};

enum arrow_keys {
    _upp = KC_UP,
    _dwn = KC_DOWN,
    _lft = KC_LEFT,
    _rgt = KC_RGHT,
    _u = KC_UP,
    _d = KC_DOWN,
    _l = KC_LEFT,
    _r = KC_RGHT,

};

enum common_shortcuts {
    undo = RCTL(_Z), save = RCTL(_S),
    cut = RCTL(_X), copy = RCTL(_C), paste = RCTL(_V),
    find = RCTL(_F),
};
enum permanent_keys {
    // To not use `oooo` fallthroughs everywhere, I'll use some aliases
    // for the keys that are used a lot and should rarely ever move / be replaced.
    _spc = KC_SPC,
    _ent = KC_ENT,
    _tab = KC_TAB,
    _stb = LSFT(KC_TAB),
    _bsp = KC_BSPC,
    _esc = KC_ESC,
    _del = KC_DEL,
    // and let's do a lot of the layer switches too
    _x2   = OSL(_HH2), // wont be needing this one prolly
    _spec = MO(_Special),
    _num  = MO(_Num),
    _sym = OSL(_Sym),
    _game = TO(_Game),
    _medi = TO(_Media),
    _os = MO(_OS_Ultra),
};

enum czech_mods {
    hacek   = LSFT(KC_EQL), // ˇ
    carka   = KC_EQL,       // ´
    krouzek = LSFT(KC_GRV), // °
    g_kase  = KC_NUHS,      // ¨
};

enum czech_shorthands {
    aaa = KC_8,     // á
    rrr = KC_5,     // ř
    sss = KC_3,     // š
    e_h = KC_2,     // ě
    e_c = KC_0,     // é
    u_c = KC_LBRC,  // úů
    iii = KC_9,     // í
    yyy = KC_7,     // ý
    u_k = KC_SCLN,  // ůú
    ccc = KC_4,     // č
    zzz = KC_6,     // ž
};
enum symbols_cz_qwerty {
    comma   = KC_COMM,          // ,
    dot     = KC_DOT,           // .
    plus    = KC_1,             // +
    minus   = KC_SLSH,          // -
    star    = RALT(KC_8),       // *
    equ     = KC_MINS,          // =
    undsc   = KC_QUES,          // _
    semic   = KC_GRV,           // ;

    dont    = LSFT(KC_NUHS),    // '
    quot    = KC_COLN,          // "
    bquot   = RALT(KC_GRV),     // `

    slash   = KC_LCBR,          //
    bslash  = RALT(KC_BSLS),    //

    par_l   = RALT(KC_9),       //
    par_r   = RALT(KC_0),       //
    curly_l = RALT(KC_LCBR),    // {
    curly_r = RALT(KC_RCBR),    // }
    brak_l  = RALT(KC_LBRC),    // [
    brak_r  = RALT(KC_RBRC),    // ]
    htm_l   = RALT(KC_COMM),    // <
    htm_r   = RALT(KC_DOT),     // >

    pipe    = RALT(KC_PIPE),    // |
    colon   = LSFT(KC_DOT), // KC_GT // :
    ques    = KC_LT,            // ?

    dollar  = RALT(KC_4),       // $
    amp     = RALT(KC_7),       // &
    at      = RALT(KC_2),       // @
    argh    = RALT(KC_1),       // !
    squig   = RALT(KC_TILD),    // ~
    perc    = RALT(KC_5),       // %
    hasht   = RALT(KC_3),       // #
    chev    = RALT(KC_6),       // ^
};

enum tap_mods {
    t_alt = OSM(MOD_LALT),
    t_gui = OSM(MOD_LGUI),
    t_sft = OSM(MOD_LSFT),
    t_ctl = OSM(MOD_RCTL),
};

// Snekys homerow mods
// Left-hand home row mods
#define GA LGUI_T(KC_A)
#define AR LALT_T(KC_R)
#define CS LCTL_T(KC_S)
#define ST LSFT_T(KC_T)

// Right-hand home row mods
#define SN RSFT_T(KC_N)
#define CE RCTL_T(KC_E)
#define AI LALT_T(KC_I)
#define GO RGUI_T(KC_O)
